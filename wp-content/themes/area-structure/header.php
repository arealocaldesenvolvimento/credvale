<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#000000">
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link
        rel="stylesheet"
        type="text/css"
        media="all"
        href="<?php echo get_template_directory_uri(); ?>/style.css"
    >
    <title>
        <?php echo is_front_page() || is_home() ? get_bloginfo('name') : wp_title(''); ?>
    </title>
    <?php wp_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-60374915-44"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-60374915-44');
    </script>
</head>
<body  <?php body_class(); ?>>
    <div class="sg-popup-id-90"></div>
    <div class="display">
        <?= do_shortcode("[sg_popup id=90]") ?>
    </div>
    <!-- Header -->
    <header role="heading" class="main-header" style="<?= get_the_post_thumbnail_url()==""?"background: #a32a31;":"background-image: url('".get_the_post_thumbnail_url()."');" ?>background-repeat: no-repeat;">
        <div class="al-menu">
            <div class="al-container">
                <div class="sub-menu">
                    <div class="logo">
                        <a title="Cred Vale" href="<?= get_site_url() ?>">
                            <img src="<?= get_field('logomarca', 'option')['url'] ?>" alt="Logo Cred">
                        </a>
                    </div>
                    <div class="main-menu">
                        <?php wp_nav_menu(array('menu'=> 'principal', 'theme_location' => 'principal', 'container' => false)); ?>
                    </div>
                </div>
            </div>
        </div>