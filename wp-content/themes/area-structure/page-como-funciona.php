<?php get_header() ?>
    <div class="al-container title-back" style="background-image: url('<?= get_the_post_thumbnail_url() ?>');">
        <h1 class="title white">Como Funciona?</h1>
    </div>
    </header>
    <!-- Wrapper -->
    <div id="wrapper">
    <div class="al-cointainer blank">
        <section class="ajuda-page">
            <h2 class="subtitle black">Saiba como a Credvale vai te ajudar!</h2>
            <ul class="cards">
                <li class="card"><article>
                    <h2 class="number">1.</h2>
                    <p class="text">Sua empresa vende a prazo em boletos ou cheques para seus clientes.</p>
                </article></li>
                <li class="card"><article>
                    <h2 class="number">2.</h2>
                    <p class="text">A CREDVALE antecipa este valor para sua empresa, mediante um baixo custo de deságio</p>
                </article></li>
                <li class="card"><article>
                    <h2 class="number">3.</h2>
                    <p class="text">Em pouco tempo, é feito o pagamento da operação na conta da sua empresa.</p>
                </article></li>
            </ul>
        </section>
    </div>
    <div class="blank">
        <div class="hr-div-a">
            <hr>
        </div>
    </div>
    <div class="faq al-container blank">
        <ul>
            <?php 
                foreach(get_field('faq') as $chave => $diferencial){
            ?>
                <li class="faqli" onclick="auto(<?= $chave ?>)">
                    <div class="pergunta">
                        <div class="line" id="line-<?= $chave ?>">
                            <?= $diferencial['pergunta'] ?>
                            <div class="img">
                                <div class="down" id="down-<?= $chave ?>">
                                    <img src="<?= get_image_url('down.png') ?>" alt="Ícone Down">
                                </div>
                                <div class="up" id="up-<?= $chave ?>">
                                    <img src="<?= get_image_url('up.png') ?>" alt="Ícone Up">
                                </div>
                            </div>
                        </div>
                        <div class="hidden" id="faq-<?= $chave ?>">
                            <?= $diferencial['resposta'] ?>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="blank banner">
        <div class="al-container mini-banner" style="background-image: url('<?= get_image_url('min-banner.png') ?>')">
            <div class="left">
                <h2 class="subtitle white">Receba à vista pelo que você vendeu a prazo!</h2>
            </div>
            <div class="right">
                <div><a href="<?= get_home_url() ?>/contato">Entrar em contato</a></div>
            </div>
        </div>
    </div>
<?php get_footer() ?>