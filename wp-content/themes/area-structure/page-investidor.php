<?php get_header() ?>
    <div class="al-container title-back" style="background-image: url('<?= get_the_post_thumbnail_url() ?>');">
        <h1 class="title white">Investidor</h1>
    </div>
    </header>
    <!-- Wrapper -->
    <div id="wrapper">
    <div class="al-container blank">
        <section class="investidor">
            <h2 class="subtitle black">Muito prazer, nós somos a <strong class="red">Credvale FIDc</strong><br> Invista conosco!</h2>
            <div class="text">
                A CREDVALE foi fundada em 2007, passando por Factoring e Securitizadora, até chegarmos ao FIDC em 2017. Iniciamos o 
                Fundo de Investimento em Direitos Creditórios já com vasta experiência no mercado, o que fez com o crescimento e sucesso da 
                empresa fosse ascendente.
            </div>
        </section>
    </div>
    <div class="trofeu al-container blank">
        <article class="card-trofeu">
            <img class="card-img" src="<?= get_image_url("trofeu.png")?>" alt="Ícone Trofeu">
            <div class="sem-criatividade">
                <p class="text-normal">Somos avaliados trimestralmente na classificação de risco pela Austing Rating, o que torna nossa inadimplência sempre mínima.</p>
                <p class="text-white">Somos avaliados trimestralmente na classificação de risco pela Austing Rating, o que torna nossa inadimplência sempre mínima.</p>
            </div>
        </article>
    </div>
    <div class="investimentos al-container blank">
        Visando a captação de recursos para ampliar e expandir os negócios, propomos as seguintes possibilidades de investimento:
        <ul class="lista-investimentos">
            <?php foreach(get_field("insvestimento") as $chave => $insvestimento){?>
                <li style="list-style-image: url(<?= get_image_url("list-img.png") ?>);" ><span class="titulo"><?= $insvestimento['titulo']?></span><span class="texto">: <?= $insvestimento['legenda']?></span></li>
            <?php }?>
        </ul>
    </div>
    <div class="blank banner">
        <div class="mini-banner-investidor blank" style="background-image: url('<?= get_image_url('min-banner.png') ?>')">
            <div class="left">
                <h2 class="subtitle white">Consulte-nos sobre esta vantajosa possibilidade de investimento financeiro.</h2>
            </div>
            <div class="right">
                <div><a href="<?= get_home_url() ?>/contato">Entrar em contato</a></div>
            </div>
        </div>
    </div>
<?php get_footer() ?>