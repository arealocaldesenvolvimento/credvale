function abrir(id){
	$("#faq-"+id).slideDown();
	$("#line-"+id).css("font-weight", "600");
	$("#up-"+id).show();
	$("#down-"+id).hide();
}
function fechar(id){
	$("#faq-"+id).slideUp();
	$("#line-"+id).css("font-weight", "500");
	$("#up-"+id).hide();
	$("#down-"+id).show();
}
function auto(id){
	if(!$("#faq-"+id).is(":visible")){
		abrir(id)
	}else{
		fechar(id)
	}
}
function showOne(id){
	$($(".left ul li")).removeClass("active");
	$($(".left ul li")[id]).addClass("active");
	$($(".right .texto")).hide();
	$($(".right .campo-"+id)).show();
}
function fecharPopUp(){
	setTimeout(()=>{
		$(".sgpb-theme-6-overlay").css("display", "none");
		$("#sgpb-popup-dialog-main-div-wrapper").css("display", "none");
	}, 70)
}