/**
 * Global theme functions
 */

/**
 * @description Exemplo ajax
 */
const exampleAjax = async () => {
	const stateResponse = await fetch(`${window.apiUrl}/estados`)
	const { states, message } = await stateResponse.json()
	console.log(states, message)
}

window.onload = async () => {
	/**
	 * Contact form 7 alerts
	 */
	const form = document.querySelector('.wpcf7')
	if (form) {
		form.addEventListener('wpcf7mailsent', () => {
			Swal.fire({
				icon: 'success',
				title: 'Sucesso!',
				text: 'Mensagem enviada!',
			})
		})

		form.addEventListener('wpcf7mailfailed', () => {
			Swal.fire({
				icon: 'error',
				title: 'Ocorreu um erro!',
				text: 'Se o erro persistir, favor contatar o suporte.',
			})
		})
	}
}
$(document).ready(()=>{
	$('.telefone span input').mask('(00) 0000-0000');
	$('.cnpj span input').mask('00.000.000/0000-00', {reverse: true});

	$(".arquivo-cliente span input").change(()=>{
		var nome = ($(".arquivo-cliente span input").val()).split("\\");
		var imprimir = nome[(nome.length-1)]==""? "ANEXAR ARQUIVO":nome[(nome.length-1)]
		$(".arquivo-cliente span #forarquivo div").html(
			"<img src='"+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/archive-icon.png'>"
			+ imprimir)
	});
	$(".arquivo-investidor span input").change(()=>{
		var nome = ($(".arquivo-investidor span input").val()).split("\\");
		var imprimir = nome[(nome.length-1)]==""? "ANEXAR ARQUIVO":nome[(nome.length-1)]
		$(".arquivo-investidor span #investidor div").html(
			"<img src='"+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/archive-icon.png'>"
			+ imprimir)
	});
	$(".arquivo-trabalho span input").change(()=>{
		var nome = ($(".arquivo-trabalho span input").val()).split("\\");
		var imprimir = nome[(nome.length-1)]==""? "ANEXAR ARQUIVO":nome[(nome.length-1)]
		$(".arquivo-trabalho span #curriculo div").html(
			"<img src='"+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/archive-icon.png'>"
			+ imprimir)
	});

	$(".hidden").hide();
	$(".up").hide();
	showOne(0);
	$('#faturamento').parent('p').css("width", "100%");

	$($('.wpcf7-file')[0]).attr('accept', '.pdf');
	$($('.wpcf7-file')[0]).attr('id', 'arquivo-input');
	$($('.wpcf7-file')[0]).css('display', 'none');
	$($('.wpcf7-file')[0]).parents('span').append("<label for='arquivo-input' id='forarquivo'><div><img src='"+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/archive-icon.png' alt='Ícone Anexo'>Anexar arquivo</div></label>");
	
	$($('.wpcf7-file')[1]).attr('accept', '.pdf');
	$($('.wpcf7-file')[1]).attr('id', 'curriculo-input');
	$($('.wpcf7-file')[1]).css('display', 'none');
	$($('.wpcf7-file')[1]).parents('span').append("<label for='curriculo-input' id='curriculo'><div><img src='"+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/archive-icon.png' alt='Ícone Anexo'>Anexar arquivo</div></label>");
	
	$($('.wpcf7-file')[2]).attr('accept', '.pdf');
	$($('.wpcf7-file')[2]).attr('id', 'investidor-input');
	$($('.wpcf7-file')[2]).css('display', 'none');
	$($('.wpcf7-file')[2]).parents('span').append("<label for='investidor-input' id='investidor'><div><img src='"+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/archive-icon.png' alt='Ícone Anexo'>Anexar arquivo</div></label>");
	
	$($(".loading")[0]).hide();
    $($(".wpcf7-submit")[0]).on("click", ()=>{          
        $($(".loading")[0]).show();
    });

	$($(".loading")[1]).hide();
    $($(".wpcf7-submit")[1]).on("click", ()=>{          
        $($(".loading")[1]).show();
    });

	$($(".loading")[2]).hide();
    $($(".wpcf7-submit")[2]).on("click", ()=>{          
        $($(".loading")[2]).show();
    });
    document.addEventListener( 'wpcf7mailsent', ()=> {
		$(".loading").hide();
    }, false );
    document.addEventListener( 'wpcf7invalid', ()=> {
		$(".loading").hide();
    }, false );
    document.addEventListener( 'wpcf7mailfailed', ()=> {
		$(".loading").hide();
    }, false );
	$(".wpcf7-spinner").remove();

	if(sessionStorage.getItem('popup')!='true'){
		setTimeout(()=>{
			$(".sg-popup-id-90").click();
		}, 100);
		sessionStorage.setItem('popup', 'true');
	}else{
		sessionStorage.setItem('popup', 'true');
	}

});