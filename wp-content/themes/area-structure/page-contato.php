<?php get_header() ?>
    <div class="al-container title-back" style="background-image: url('<?= get_the_post_thumbnail_url() ?>');">
        <h1 class="title white">Contato</h1>
    </div>
    </header>
    <!-- Wrapper -->
    <div id="wrapper">
    <div class="contato al-container blank">
        <h2 class="subtitle black">Selecione a opção que deseja: </h2>
        <div class="contato-row">
            <div class="left">
                <ul>
                    <li onclick="showOne(0)">Atendimento ao cliente <img src="<?= get_image_url('flecha-ativa.png')?>" alt="Ícone Flecha"></li>
                    <li onclick="showOne(1)">Trabalhe conosco <img src="<?= get_image_url('flecha-ativa.png')?>" alt="Ícone Flecha"></li>
                    <li onclick="showOne(2)">Seja um investidor <img src="<?= get_image_url('flecha-ativa.png')?>" alt="Ícone Flecha"></li>
                </ul>
                <div class="hr-div">
                    <hr class="hr">
                </div>
                <div class="outras">
                    <h2 class="subtitle black">Outras opções de contato</h2>
                    <table class="lista atendimento">
                        <tr>
                            <td><img src="<?= get_image_url('phone.png'); ?>" alt="Phone icon" id="phone"></td>
                            <td><a class="link" href="tel: <?= get_field('telefone', 'option')['back']?>">
                                <?= get_field('telefone', 'option')['front']?>
                            </a></td>
                        </tr>
                        <tr>
                            <td><img src="<?= get_image_url('mail.png'); ?>" alt="E-mail icon" id="mail"></td>
                            <td><a class="link" href="mailto:<?= get_field('email', 'option')?>"><?= get_field('email', 'option')?></a></td>
                        </tr>
                    </table><br><br>
                </div>
            </div>
            <div class="right">
                <div class="texto campo-0 cliente">
                    <h2 class="subtitle-red">Atendimento ao cliente</h2>
                    <span>Para fazer cadastro junto a CREDVALE é muito fácil! Basta preencher os campos abaixo, se você já possuir em mãos os documentos para abertura de cadastro pode enviar que logo entraremos em contato.
                    </span>
                    <p>Documentos necessários para abertura de cadastro:</p>
                    <ul>
                        <li>- Contrato Social (cópia simples).</li>
                        <li>- RG, CPF, (cópia simples) dos Sócios(as) .</li>
                        <li>- Comprovante de residência (cópia simples) dos Sócios(as) .</li>
                        <li>- Faturamento últimos 12 meses.</li>
                    </ul>
                    <?= do_shortcode('[contact-form-7 id="77" title="Cliente"]')?>
                </div>
                <div class="texto campo-1 trabalho">
                    <h2 class="subtitle-red">Trabalhe conosco</h2>
                    <p>Preencha os campos abaixo, entraremos em contato em breve!</p>
                    <?= do_shortcode('[contact-form-7 id="78" title="Trabalho"]')?>
                </div>
                <div class="texto campo-2 investidor">
                    <h2 class="subtitle-red">Seja um investidor</h2>
                    <br>
                    <span>A CREDVALE oferece investimentos de alta qualidade, baixo risco e ótimo retorno. São alternativas avaliadas positivamente 
                        pelas agências de classificação de risco e que garantem segurança e rendimentos acima da média de mercado.</span>
                    <p>Insvestimento:</p>
                    <span>Visando a captação de recursos para ampliar e expandir os negócios, propomos as seguintes possibilidades de investimento:</span>
                    <ul>
                        <li>- Garantias: Responsabilidade civil da CREDVALE e seus sócios. Além deste, após aporte do investimento junto a CREDVALE , lastreamos o seu recurso aos títulos negociados, isentando o risco de inadimplência, ficando este por conta da CREDVALE, caso não sejam quitados.</li>
                    </ul>
                    <p>Segurança:</p>
                    <span>Objetivamos uma parceria comercial e financeira sólida, rentável, crescente e que proporcione aos investidores satisfação e lucratividade compatíveis com a expectativa de retorno financeiro durante o período de vigência deste.
                    <br>Consulte-nos sobre esta vantajosa possibilidade de investimento financeiro.</span>
                    <p>Para saber mais como podemos ajudá-lo, preencha o formulário abaixo que entraremos em contato com você.</p>
                    <?= do_shortcode('[contact-form-7 id="76" title="investidor"]')?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>