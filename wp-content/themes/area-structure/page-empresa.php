<?php get_header() ?>
    <div class="al-container title-back banner-pequeno" style="background-image: url('<?= get_the_post_thumbnail_url() ?>');">
        <h1 class="title white">Empresa</h1>
    </div>
    </header>
    <!-- Wrapper -->
    <div id="wrapper">
    <div class="al-container blank">
        <div class="conheca">
            <section class="texto">
                <h2 class="subtitle black">Conheça nossa história de <i><strong class="red">CREDI</strong>BILIDADE</i></h2>
                <p>A CREDVALE é uma empresa Catarinense, com sede na região do Alto Vale do Itajaí, na cidade de Rio do Sul. 
                Foi fundada em 2007, e desde então, atua no segmento de antecipação de recebíveis, com desconto de duplicatas e troca de cheques.</p>
                <p>No ano de 2017, iniciaram-se as operações no Fundo de Investimento em Direitos Creditórios, o FIDC. Neste momento, a 
                CREDVALE passa a ofertar taxas mais atrativas no mercado para seus clientes, e receber mais investimentos de renda fixa.</p>
                <p>Em 2020, a CREDVALE FIDC mudou-se para sede própria: um amplo prédio comercial com sete andares disponíveis para acompanhar o crescimento da empresa.</p>
                <p>Nossos parceiros nesta missão são a <strong class="red">Ouro Preto Investimentos</strong>, nossa gestora, e a <strong class="red">Singulare</strong>, nossa administradora.</p>
            </section>
            <section class="fachada">
                <img src="<?= get_image_url("fachada.png") ?>" alt="Imagem Fachada">
            </section>
        </div>
    </div>
    <div class="missao al-container blank">
        <article class="card">
            <img src="<?= get_image_url("mountain.png")?>" alt="Ícone Montanha">
            <h2 class="subtitle black">Missão</h2>
            <p>A política da empresa prioriza sempre o cliente, por atendimento com maior agilidade, transparência e ética.</p>
        </article>
    </div>
    <div class="diferenciais al-container blank">
        <h2 class="subtitle black">Nossos diferenciais<br>competitivos</h2>
        <ul>
            <?php 
                foreach(get_field('diferenciais') as $chave => $diferencial){
            ?>
                <li><strong class="red"><?= $diferencial['titulo'] ?>:</strong><?= $diferencial['legenda'] ?></li>
            <?php } ?>
        </ul>
    </div>
    <div class="al-container video blank">
        <iframe style="border: 1px black solid;box-shadow: 0 0 7px 0 rgb(0 0 0);" width="680" height="382" src="https://www.youtube.com/embed/ZEoY4JNCst8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
<?php get_footer() ?>