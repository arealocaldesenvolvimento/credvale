<?php get_header() ?>

    <div class="al-container banner-grande">
        <section class="capa">
            <h2 class="subtitle white">Antecipação de cheques, boletos e duplicatas!</h2>
            <p class="legend white">Sua empresa tem recebíveis futuros, tem dinheiro à vista na CREDVALE</p>
            <div class="cont-btn"><a href="<?= get_home_url() ?>/como-funciona"><div class="btn-yellow">Conheça mais!</div></a></div>
        </section>
    </div>
    </header>
    <!-- Wrapper -->
    <div id="wrapper">
    <div class="help al-cointainer">
        <section class="ajuda">
            <h2 class="subtitle black">Saiba como a Credvale vai te ajudar!</h2>
            <ul class="cards">
                <li class="card"><article>
                    <h2 class="number">1.</h2>
                    <p class="text">Sua empresa vende a prazo em boletos ou cheques para seus clientes.</p>
                </article></li>
                <li class="card"><article>
                    <h2 class="number">2.</h2>
                    <p class="text">A CREDVALE antecipa este valor para sua empresa, mediante um baixo custo de deságio</p>
                </article></li>
                <li class="card"><article>
                    <h2 class="number">3.</h2>
                    <p class="text">Em pouco tempo, é feito o pagamento da operação na conta da sua empresa.</p>
                </article></li>
            </ul>
        </section>
    </div>
    <div class="info">
        <div class="img">
            <img src="<?= get_image_url('faixa-lateral-home.png') ?>" alt="Imagem Banner Lateral">
        </div>
        <div class="texto">
            <h2 class="subtitle black">Conheça nossa história de <i><strong class="red">CREDI</strong>BILIDADE</i></h2>
            <p class="text">
                A CREDVALE é uma empresa Catarinense, com sede na região do Alto Vale do Itajaí, na cidade de Rio do Sul. Foi fundada em 2007, 
                e desde então, atua no segmento de antecipação de recebíveis, com desconto de duplicatas e troca de cheques.
            </p>
            <div class="cont-btn"><a href="<?= get_home_url() ?>/empresa"><div class="btn-yellow">Saiba mais!</div></a></div>
        </div>
    </div>
    <div class="al-container assoc">
        <div class="associados">
            <ul class="lista">
                <?php foreach(get_field("associados") as $chave => $associado){?>
                    <li><img src="<?= $associado['imagem']['url']?>" alt="<?= $associado['imagem']['title']?>"></li>
                <?php }?>
            </ul>
        </div>
    </div>
<?php get_footer() ?>