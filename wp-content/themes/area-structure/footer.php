    </div>

    <!-- Footer -->
    <footer class="main-footer">
        <div class="sup-footer">
            <div class="al-container">
                <div class="collum-footer">
                    <div class="logo">
                        <a title="Linsul" href="<?= get_site_url() ?>">
                            <img src="<?= get_field('logomarca', 'option')['url'] ?>" alt="Logo Linsul">
                        </a>
                    </div>
                </div>
                <div class="collum-footer">
                    <span class="footer-title">Páginas</span>
                    <ul class="lista">
                        <li><a class="link" href="<?= get_home_url() ?>/home">Home</a></li>
                        <li><a class="link" href="<?= get_home_url() ?>/empresa">Conheça nossa empresa</a></li>
                        <li><a class="link" href="<?= get_home_url() ?>/como-funciona">Como funciona</a></li>
                        <li><a class="link" href="<?= get_home_url() ?>/investidor">Investidor</a></li>
                    </ul>
                </div>
                <div class="collum-footer">
                    <span class="footer-title">Atendimento</span>
                    <table class="lista atendimento">
                        <tr>
                            <td><img src="<?= get_image_url('phone.png'); ?>" alt="Phone icon" id="phone"></td>
                            <td><a class="link" href="tel: <?= get_field('telefone', 'option')['back']?>">
                                <?= get_field('telefone', 'option')['front']?>
                            </a></td>
                        </tr>
                        <tr>
                            <td><img src="<?= get_image_url('mail.png'); ?>" alt="E-mail icon" id="mail"></td>
                            <td><a class="link" href="mailto:<?= get_field('email', 'option')?>"><?= get_field('email', 'option')?></a></td>
                        </tr>
                    </table><br><br>
                </div>
                <div class="collum-footer">
                    <span class="footer-title">Endereço</span>
                    <table class="lista endereco">
                        <tr>
                            <td><img src="<?= get_image_url('map.png'); ?>" alt="Map icon" id="map"></td>
                            <td>
                                <a target="_blank" class="link" href="<?= get_field('link_de_endereco', 'option')?>">
                                Rua Princesa Isabel, nº 681, Andar 2 –<br>Sala 201 Bairro Canoas - Rio do Sul/SC  -<br>CEP 89.164-054</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="collum-footer">
                    <ul class="lista blue-icons">
                        <a href="<?= get_field('instagram', 'option')?>" target="_blank"><li class="redirect-instagram"><img src="<?= get_image_url('instagram.png'); ?>" alt="Ícone Instagram "></li></a><br>
                        <a href="<?= get_field('facebook', 'option')?>" target="_blank"><li class="redirect-facebook"><img src="<?= get_image_url('facebook.png'); ?>" alt="Ícone Facebook "></li></a><br>
                        <a href="<?= get_field('linkedin', 'option')?>" target="_blank"><li class="redirect-linkedin"><img src="<?= get_image_url('linkedin.png'); ?>" alt="Ícone Linkedin "></li></a><br>
                    </ul>
                </div>
            </div>
        </div>
        <div class="sub-footer">
            <div class="al-container">Desenvolvido por: <a href="https://www.arealocal.com.br/">Área Local</a></div>
        </div>
    </footer>

    <!-- Scripts -->
    <script>
        /**
         * @description global JS variables
         */
        window.alUrl = {
            templateUrl: '<?php echo addslashes(get_bloginfo('template_url')); ?>',
            homeUrl: '<?php echo addslashes(home_url()); ?>'
        }
	    window.apiUrl = `${window.alUrl.homeUrl}/wp-json/api`
    </script>
    <script src="https://kit.fontawesome.com/4800576786.js" crossorigin="anonymous"></script>

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/theme/funcoes.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/js/vendor.js"></script>
    <script async type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/js/app.js"></script>
    <?php wp_footer(); ?>
    
</body>
</html>
